#include "Display.h"
#include <GL/glew.h>
#include <iostream>

Display::Display(int width, int height, std::string& title)
{
	SDL_Init(SDL_INIT_EVERYTHING);

	//SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	//SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	//SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	//SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	//SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
	//SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
	m_glContext = SDL_GL_CreateContext(m_window);
	
	GLenum	status = glewInit();

	if (status != GLEW_OK)
	{
		std::cerr << "Failed of glew initializion!" << std::endl;
	}

	m_isClosed = false;
}

Display::~Display()
{
	SDL_GL_DeleteContext(m_glContext);
	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

void	Display::ClearBuffer(float red, float green, float blue, float alpha)
{
	glClearColor(red, green, blue, alpha);
	glClear(GL_COLOR_BUFFER_BIT);
}

bool	Display::GetIsClosed()
{
	return m_isClosed;
}

void	Display::SwapBuffer()
{
	SDL_Event	event;
	SDL_GL_SwapWindow(m_window);

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			m_isClosed = true;
	}
}