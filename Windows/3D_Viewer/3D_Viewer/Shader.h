#ifndef SHADER_H
# define SHADER_H
# include <string>
# include <GL/glew.h>

class Shader
{
public:
	Shader();
	Shader(const std::string& path);

	void	BindProgram();

	virtual ~Shader();
private:
	static const unsigned int	NUM_SHADERS = 2;
	Shader(const Shader& src) { *this = src; }
	void operator=(const Shader& rhs) {}

	GLuint						m_program;
	GLuint						m_shaders[NUM_SHADERS];
};
#endif // !SHADER_H
