#ifndef DISPLAY_H
# define DISPLAY_H
# include <string>
# include <SDL2/SDL.h>

class Display
{
public:
	Display() {}
	Display(int width, int height, std::string& title);

	void			SwapBuffer();
	bool			GetIsClosed();
	void			ClearBuffer(float red, float green, float blue, float alpha);

	virtual ~Display();
private:
	Display(const Display& src) { *this = src; }
	void operator=(const Display& rhs) {}

	SDL_Window*		m_window;
	SDL_GLContext	m_glContext;
	bool			m_isClosed;
};

#endif // !DISPLAY_H