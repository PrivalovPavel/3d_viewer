#include <iostream>
#include <GL/glew.h>
#include <string>
#include "Display.h"
#include "Shader.h"

#define W 800
#define H 600

int		main(int argc, char** argv)
{
	std::string	title = "3D Viewer";
	Shader		shader("./shaders/shader");
	Display		display(W, H, title);

	while (!display.GetIsClosed())
	{
		display.ClearBuffer(0.3f, 0.3f, 0.3f, 1.0f);
		shader.BindProgram();

		display.SwapBuffer();
	}
	return 0;
}