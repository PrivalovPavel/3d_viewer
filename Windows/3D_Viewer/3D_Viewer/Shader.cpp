#include "Shader.h"
#include <iostream>
#include <fstream>

static		 GLuint CreateShader(const std::string& src, GLenum type);
static		std::string LoadShader(std::string const &fileName);
static void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, std::string const &errorMes);

Shader::Shader(const std::string& path)
{
	m_program = glCreateProgram();
	m_shaders[0] = CreateShader(path + ".vs", GL_VERTEX_SHADER);
	m_shaders[1] = CreateShader(path + ".fs", GL_FRAGMENT_SHADER);

	for (unsigned int i = 0; i < NUM_SHADERS; i++)
		glAttachShader(m_program, m_shaders[i]);

	glLinkProgram(m_program);
	CheckShaderError(m_program, GL_LINK_STATUS, true, "Error: Failed program linking shader");

	glValidateProgram(m_program);
	CheckShaderError(m_program, GL_VALIDATE_STATUS, true, "Error: Failed program validating");
}

Shader::~Shader()
{
	for (unsigned int i = 0; i < NUM_SHADERS; i++)
	{
		glDetachShader(m_program, m_shaders[i]);
		glDeleteShader(m_shaders[i]);
	}

	glDeleteProgram(m_program);
}

static GLuint CreateShader(const std::string& src, GLenum type)
{
	GLuint			shader = glCreateShader(type);
	const GLchar*	shaderString[1];
	GLint			shaderStringLength[1];

	if (shader == 0)
		std::cerr << "Error: Shader creation failed" << std::endl;

	shaderString[0] = src.c_str();
	shaderStringLength[0] = src.length();

	glShaderSource(shader, 1, shaderString, shaderStringLength);
	glCompileShader(shader);

	CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error: Failed shader compiling");

	return shader;
}

void	Shader::BindProgram()
{
	glUseProgram(m_program);
}

static std::string LoadShader(std::string const &fileName)
{
	std::ifstream	file;
	std::string		res;
	std::string		line;

	file.open(fileName);
	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			res += line + "\n";
		}
	}
	else {
		std::cerr << "Can not open shader file: '" + fileName + "'"  << std::endl;
	}
	return res;
}

static void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, std::string const &errorMes)
{
	GLint	success = 0;
	GLchar	error[1024] = { 0 };

	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);
		std::cerr << errorMes << ": '" << error << "'" << std::endl;
	}
}
